//
//  TagsData+CoreDataProperties.swift
//  CoreDataChallenge
//
//  Created by Михаил on 19.04.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//
//

import Foundation
import CoreData


extension TagsData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TagsData> {
        return NSFetchRequest<TagsData>(entityName: "TagsData")
    }

    @NSManaged public var tag: String?
    @NSManaged public var personT: PersonData?

    public var wrappedTag: String {
        tag ?? "Unknown tag"
    }
}
