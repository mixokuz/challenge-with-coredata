//
//  FriendData+CoreDataProperties.swift
//  CoreDataChallenge
//
//  Created by Михаил on 19.04.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//
//

import Foundation
import CoreData


extension FriendData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FriendData> {
        return NSFetchRequest<FriendData>(entityName: "FriendData")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var personF: PersonData?

    public var wrappedId: UUID {
        id ?? UUID()
    }
    
    public var wrappedName: String {
        name ?? "Unknown name"
    }
}
