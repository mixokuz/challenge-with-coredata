//
//  PersonData+CoreDataProperties.swift
//  CoreDataChallenge
//
//  Created by Михаил on 19.04.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//
//

import Foundation
import CoreData


extension PersonData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PersonData> {
        return NSFetchRequest<PersonData>(entityName: "PersonData")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var age: Int16
    @NSManaged public var isActive: Bool
    @NSManaged public var company: String?
    @NSManaged public var email: String?
    @NSManaged public var address: String?
    @NSManaged public var about: String?
    @NSManaged public var registered: String?
    @NSManaged public var tags: NSSet?
    @NSManaged public var friends: NSSet?

    public var wrappedId: UUID {
        id ?? UUID()
    }
    
    public var wrappedName: String {
        name ?? "Unknown name"
    }
    
    public var wrappedCompany: String {
        company ?? "Unknown company"
    }
    
    public var wrappedEmail: String {
        email ?? "Unknown email"
    }
    
    public var wrappedAddress: String {
        address ?? "Unknown address"
    }
    
    public var wrappedAbout: String {
        about ?? "Unknown about"
    }
    
    public var wrappedRegistered: String {
        registered ?? "Unknown registered"
    }
    
    public var tagsArray: [TagsData] {
        let set = tags as? Set<TagsData> ?? []
        return set.sorted {
            $0.wrappedTag < $1.wrappedTag
        }
    }
    
    public var friendsArray: [FriendData] {
        let set = friends as? Set<FriendData> ?? []
        return set.sorted {
            $0.wrappedName < $1.wrappedName
        }
    }
}

// MARK: Generated accessors for tags
extension PersonData {

    @objc(addTagsObject:)
    @NSManaged public func addToTags(_ value: TagsData)

    @objc(removeTagsObject:)
    @NSManaged public func removeFromTags(_ value: TagsData)

    @objc(addTags:)
    @NSManaged public func addToTags(_ values: NSSet)

    @objc(removeTags:)
    @NSManaged public func removeFromTags(_ values: NSSet)

}

// MARK: Generated accessors for friends
extension PersonData {

    @objc(addFriendsObject:)
    @NSManaged public func addToFriends(_ value: FriendData)

    @objc(removeFriendsObject:)
    @NSManaged public func removeFromFriends(_ value: FriendData)

    @objc(addFriends:)
    @NSManaged public func addToFriends(_ values: NSSet)

    @objc(removeFriends:)
    @NSManaged public func removeFromFriends(_ values: NSSet)

}
