//
//  Tags+CoreDataProperties.swift
//  CoreDataChallenge
//
//  Created by Михаил on 19.04.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//
//

import Foundation
import CoreData


extension Tags {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tags> {
        return NSFetchRequest<Tags>(entityName: "Tags")
    }

    @NSManaged public var tag: String?
    
    public var wrappedTag: String {
        tag ?? "Unknown tag"
    }

}
