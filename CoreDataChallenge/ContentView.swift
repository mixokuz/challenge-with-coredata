//
//  ContentView.swift
//  CoreDataChallenge
//
//  Created by Михаил on 10.04.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//

import SwiftUI
import CoreData


struct Person : Codable, Identifiable {
    var id: UUID
    var isActive : Bool
    var name : String
    var age : Int
    var company : String
    var email : String
    var address : String
    var about : String
    var registered : String
    var tags : [String]
    
    struct Friend : Codable {
        var id : UUID
        var name : String
    }

    var friends : [Friend]
    
}

class People: Codable {
    var people:[Person] = []
}

struct ContentView: View {
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(entity: PersonData.entity(), sortDescriptors: []) var persons: FetchedResults<PersonData>
    
    @State private var people = [Person]()
    
    var body: some View {
        VStack {
            List {
                ForEach(persons, id: \.self) { person in
                    Section(header: Text(person.wrappedName)) {
                        ForEach(person.friendsArray, id: \.self) { tag in
                            Text(tag.wrappedName)
                        }
                    }
                    
                }
            }
            
            Button("FFF") {
                for number in self.people {
                    let person = PersonData(context: self.moc)
                    person.name = number.name
                    person.age = Int16(number.age)
                    person.about = number.about
                    person.address = number.address
                    person.company = number.company
                    person.email = number.email
                    person.registered = number.registered
                    person.isActive = number.isActive
                    person.id = number.id
                    try? self.moc.save()
                    
                    for tag in number.tags {
                        let oneTag = TagsData(context: self.moc)
                        oneTag.tag = tag
                        
                        oneTag.personT = PersonData(context: self.moc)
                        oneTag.personT?.name = number.name
                        oneTag.personT?.age = Int16(number.age)
                        oneTag.personT?.about = number.about
                        oneTag.personT?.address = number.address
                        oneTag.personT?.company = number.company
                        oneTag.personT?.email = number.email
                        oneTag.personT?.registered = number.registered
                        oneTag.personT?.isActive = number.isActive
                        oneTag.personT?.id = number.id
                        try? self.moc.save()
                    }
                    
                    for friend in number.friends {
                        let oneFriend = FriendData(context: self.moc)
                        oneFriend.name = friend.name
                        oneFriend.id = friend.id
                        oneFriend.personF = PersonData(context: self.moc)
                        oneFriend.personF?.name = number.name
                        oneFriend.personF?.age = Int16(number.age)
                        oneFriend.personF?.about = number.about
                        oneFriend.personF?.address = number.address
                        oneFriend.personF?.company = number.company
                        oneFriend.personF?.email = number.email
                        oneFriend.personF?.registered = number.registered
                        oneFriend.personF?.isActive = number.isActive
                        oneFriend.personF?.id = number.id
                        try? self.moc.save()
                    }
                    

                }
                
            }
        }.onAppear(perform: loadData)
    }
    
    func loadData() {
        
    guard let url = URL(string: "https://www.hackingwithswift.com/samples/friendface.json") else {
        print("Invalid URL")
        return
    }
    
    let request = URLRequest(url: url)
    
    let decoder = JSONDecoder()
    
    URLSession.shared.dataTask(with: request) { data, response, error in
        print(data!)
        guard let json = data else {
            print("No data received")
            return
        }
        do {
            let decodedResponse = try decoder.decode([Person].self, from: json)
            DispatchQueue.main.async {
                self.people = decodedResponse
            }
            return
        } catch {
            print("Fetch failed: \(error.localizedDescription)")
            }
        }.resume()
    }
    
//the version without CoreData
//    struct ContentView: View {
//    @State private var people = [Person]()
//    var peopleCommon = People()
//
//    var body: some View {
//        NavigationView {
//            List(people, id: \.id) { person in
//                NavigationLink(destination: PersonView(person: person, friends: self.people, people: self.people)) {
//                    Text(person.name)
//                    .font(.headline)
//                }
//            }
//            .onAppear(perform: loadData)
//        }
//    }
//
//    func loadData() {
//
//        guard let url = URL(string: "https://www.hackingwithswift.com/samples/friendface.json") else {
//            print("Invalid URL")
//            return
//        }
//
//        let request = URLRequest(url: url)
//
//        let decoder = JSONDecoder()
//
//        URLSession.shared.dataTask(with: request) { data, response, error in
//            print(data!)
//            guard let json = data else {
//                print("No data received")
//                return
//            }
//            do {
//                let decodedResponse = try decoder.decode([Person].self, from: json)
//                    DispatchQueue.main.async {
//                        self.people = decodedResponse
//                        self.peopleCommon.people = decodedResponse
//                    }
//                    return
//            } catch {
//                    print("Fetch failed: \(error.localizedDescription)")
//            }
//        }.resume()
//    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//the version without CoreData
//struct Response : Codable {
//    var results: [Person]
//}
//
////
extension Bundle {
    func decode<T: Codable>(_ file: String) -> T {

        guard let url = self.url(forResource: file, withExtension: nil) else {
            fatalError("Failed to locate \(file) in bundle.")
        }
        print("Ok")
        guard let data = try? Data(contentsOf: url) else {
            fatalError("Failed to load \(file) from bundle.")
        }

        let decoder = JSONDecoder()

        guard let loaded = try? decoder.decode(T.self, from: data) else {
            fatalError("Failed to decode \(file) from bundle.")
        }

        return loaded
    }
}
//
//struct PersonView: View {
//
//    struct Friend {
//        let id : UUID
//        let friend : Person
//    }
//    let people : [Person]
//    let person : Person
//    let friends: [Friend]
//
//    var body : some View {
//        GeometryReader { geometry in
//            ScrollView(.vertical) {
//                VStack {
//                    HStack {
//                        Text(self.person.name)
//                        Text(String(self.person.age))
//                    }
//                    .font(.largeTitle)
//                    Text(self.person.company)
//
//                    Text(self.person.about)
//                    ForEach (self.friends, id: \.id) { friend in
//                        NavigationLink(destination: PersonView(person: self.people.first(where: {$0.id == friend.id})!, friends: self.people, people: self.people)) {
//                            Text(friend.friend.name)
//                        }
//
////                        NavigationLink(destination: friends.first(where: {$0.id == friend.id}) ?
////                            PersonView(person: people.first(where: {$0.id == friend.id}) , friends: self.people) : Text("Nothing")  ) {
////                            Text("Name")
////                        }
//                    }
//                }
//            }
//        }
//    }
//
//
//
//    init(person: Person, friends: [Person], people: [Person]) {
//        self.person = person
//        var matches = [Friend]()
//        self.people = people
//
//        for member in person.friends {
//            if let match = friends.first(where: {$0.id == member.id}) {
//                matches.append(Friend(id: member.id, friend: match))
//            } else {
//                fatalError("Missing friend \(member)" )
//            }
//        }
//
//        self.friends = matches
//    }
//
//}
