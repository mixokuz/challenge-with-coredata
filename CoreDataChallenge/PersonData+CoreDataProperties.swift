//
//  PersonData+CoreDataProperties.swift
//  CoreDataChallenge
//
//  Created by Михаил on 19.04.2020.
//  Copyright © 2020 Михаил. All rights reserved.
//
//

import Foundation
import CoreData


extension PersonData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PersonData> {
        return NSFetchRequest<PersonData>(entityName: "PersonData")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var age: Int16
    @NSManaged public var isActive: Bool
    @NSManaged public var company: String?
    @NSManaged public var email: String?
    @NSManaged public var address: String?
    @NSManaged public var about: String?
    @NSManaged public var registered: String?
    @NSManaged public var tags: Tags?
    @NSManaged public var friends: FriendData?

    public var wrappedId: UUID {
        tag ?? "Unknown id"
    }
    
    public var wrappedName: String {
        name ?? "Unknown name"
    }
    
    public var wrappedCompany: String {
        company ?? "Unknown company"
    }
    
    public var wrappedEmail: String {
        email ?? "Unknown email"
    }
    
    public var wrappedAddress: String {
        address ?? "Unknown address"
    }
    
    public var wrappedAbout: String {
        about ?? "Unknown about"
    }
    
    public var wrappedRegistered: String {
        registered ?? "Unknown registered"
    }
    
    public var wrappedTag: Tags {
        tags ?? "Unknown tags"
    }
    
    public var wrappedFriends: FriendData {
        FriendData ?? "Unknown friends"
    }
}
